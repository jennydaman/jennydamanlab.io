const meshOverlaySettings = {
  colormap: 'jet',
  cal_min: 0.0,
  cal_max: 6.0,
  opacity: 1.0
};

const meshes = [
  {
    url: '../assets/data/subj_age29ga/lh.white_matter_81920.mz3',
    layers: [
      {
        url: '../assets/data/subj_age29ga/lh.subplate.tlink_10mm.mz3',
        ...meshOverlaySettings,
      }
    ]
  },
  {
    url: '../assets/data/subj_age29ga/rh.white_matter_81920.mz3',
    layers: [
      {
        url: '../assets/data/subj_age29ga/rh.subplate.tlink_10mm.mz3',
        ...meshOverlaySettings,
      }
    ]
  }
];

const showDemoButton = document.getElementById('show-niivue-demo-btn');
const nvCanvasAndCaption = document.getElementById('niivue-canvas-and-caption');

showDemoButton.onclick = () => {
  showDemoButton.style.display = 'none';
  nvCanvasAndCaption.style.display = 'block';
  showDemo();
}
showDemoButton.style.display = 'block';  // make button visible

async function showDemo() {
  // use dynamic import for lazy loading, i.e. 2.4MB file does not get loaded if button not clicked.
  const niivue = await import('./vendor/niivue-v0.38.8.min.js');
  const nv = new niivue.Niivue();
  nv.attachTo('nv');
  nv.setHighResolutionCapable(false);
  await nv.loadMeshes(meshes);
}
