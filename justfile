install:
	cargo install --locked dev-serve

dev: dev-serve

dev-serve:
	dev-serve --reload --verbose src

dev-py:
	python -m http.server -d src 3000

resume:
	cd latex && pdflatex -interaction=nonstopmode resume.tex
	mv -v latex/resume.pdf src/resume.pdf
