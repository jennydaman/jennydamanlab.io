# Jennings Zhang's Personal Website

The coolest college kids have the most colorful, animated websites, ever, whereas
60-year-old professors use HTML and maybe a touch of CSS. Where do I stand?

...

## Inspirations

- https://html5boilerplate.com/
- https://thebestmotherfucking.website/
- https://www.gnu.org/philosophy/javascript-trap.en.html
