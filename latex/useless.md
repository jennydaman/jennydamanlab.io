
# Too old to be relevant

## aimHI

Food and Drug Administration

Summer 2016

> Product Manager

- Incubator program
- Agile development
- Researched mobile solutions for improving the management of postural orthostatic tachycardia syndrome (POTS).


## EdX Massive Open Online Courses (MOOC)

- MIT LaunchX
  - August 2016
  - Becoming an Entrepreneur
    - Product formulation and  market strategy
- Microsoft DAT208x
  - March 2017
  - Introduction to Python for Data Science
